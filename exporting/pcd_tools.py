import subprocess
    
def view_pcd(filepath):
    """Displays the pointcloud in pcl viewer
    """
    display_available = bool(os.environ.get('DISPLAY', None))
    
    if display_available:
        view_command = ("pcl_viewer " + filepath).split()
        subprocess.run(view_command)
    else:
        print("ERROR: Display unavailable")
