# 3D-mapper

This project aims to demonstrate how to 3D map an area using the Intel Realsense D415 depth camera and a 360 degree feedback motor.

## Hardware
The hardware used in this project is listed below:

* Intel Realsense D415
* Raspberry Pi 4
* Arduino Nano
* Adafruit 360 degree feedback servo

## Software requirements
The code within this project requires the following software and libraries:
* ROS-Melodic
* realsense-ros
* rtabmap_ros
* Python 3
* Ubuntu 18.04

## The program
The code can be run using the run_mapper script which accepts several parameters, details of which can be found by passing the -h flag upon execution of the script. 

Upon running the program the servo will rotate allowing the camera to get a full 360 degree view of its environment, the servo will then rotate back to its orginal position. These rotations are repeated a specified number of times before the pointcloud created by the mapping is exported to the pointclouds folder. The program does offer a feature for viewing the pointcloud, however, does require some form of display.

## 3D printing
During the project I designed and created an enclosure for all the hardware components to be held in. This prevented wires becoming tangled and made the overall system much more stable. I have provided the stl files in this repository.

## Improvements
There are multiple improvements which could be carried out on this project such as making the enclosure for the system much more efficient and robust. 

During the implementation of the program I aimed to have the program render and export the pointcloud as a mesh using the open3d library, however, I discovered that open3d does not currently work on the ARM architecture present on the Raspberry Pi 4. I therefore had to abandon this part of the system, however, it could be implemented from the ground up without the open3d library.

## Example pointcloud
![](https://i.imgur.com/vxvUQBJ.png)

## The system
![](https://i.imgur.com/IMu1lBM.jpg)



