import subprocess
import os
import signal
from pathlib import Path

def start_process(cmd, cmd_type, hide_output=True, directory="."):
    """Runs a provided shell command as a background process, returns the command process.

    Arguments:
        cmd {string} -- The command to run
        cmd_type {string} -- The alias with the command allowing for neater logging
    """
    FNULL = open(os.devnull, 'w')
    print("Starting ", cmd_type)
    cmd_list = cmd.split()

    if hide_output:
        return subprocess.Popen(cmd_list, stdout=FNULL, stderr=subprocess.STDOUT, cwd=directory)
    else:
        return subprocess.Popen(cmd_list, cwd=directory)

def run_realsense():
    """Starts the realsense node

    Returns:
        Subprocess -- The process for the realsense node
    """
    realsense_command = "roslaunch realsense2_camera rs_camera.launch align_depth:=true"
    return start_process(realsense_command, "Realsense")

def run_rtabmap(no_display=True, debug=False):
    """Launches RTAB-MAP

    Returns:
        Subprocess -- The process for the RTAB-MAP node
    """
    if no_display:
        rtabmap_command = """roslaunch rtabmap_ros rtabmap.launch rtabmap_args:=\"--delete_db_on_start\" 
                            depth_topic:=/camera/aligned_depth_to_color/image_raw 
                            rgb_topic:=/camera/color/image_raw camera_info_topic:=/camera/color/camera_info 
                            approx_sync:=false rviz:=false rtabmapviz:=false"""
    else:
        rtabmap_command = """roslaunch rtabmap_ros rtabmap.launch rtabmap_args:=\"--delete_db_on_start\" 
                            depth_topic:=/camera/aligned_depth_to_color/image_raw 
                            rgb_topic:=/camera/color/image_raw camera_info_topic:=/camera/color/camera_info 
                            approx_sync:=false"""

    return start_process(rtabmap_command, "RTAB-MAP", not debug)

def get_pointcloud():
    """Exports the pointcloud from the RTAB-MAP process into the pointclouds subdirectory

    Returns:
        string -- Returns the path to the pointcloud exported from RTAB-MAP
    """
    POINTCLOUD_PATH = os.path.join(os.getcwd(), "pointclouds")
    NAME_INDEX = 0
    TIME_INDEX = 1
    CAPTURE_COMMAND = "rosrun pcl_ros pointcloud_to_pcd input:=/rtabmap/cloud_map"

    # Checks if the pointclouds directory exists, if not, is created
    Path("pointclouds").mkdir(parents=True, exist_ok=True)
    
    # Getting list of current pcds in the pointcloud directory
    existing_pcds = get_current_files(POINTCLOUD_PATH)
    capture_proc = start_process(CAPTURE_COMMAND, "Pointcloud Capture", directory=POINTCLOUD_PATH)

    # Checking if the capture process has saved a new pcd to the pointcloud directory
    new_pcds = get_current_files(POINTCLOUD_PATH)
    while (new_pcds == existing_pcds):
        new_pcds = get_current_files(POINTCLOUD_PATH)
    os.kill(capture_proc.pid, signal.SIGINT)

    # Iterating through the new pcds from the directory and selecting the most recent one
    most_recent = ()
    for pcd in (new_pcds  - existing_pcds):
        if not most_recent or pcd[TIME_INDEX] < most_recent[TIME_INDEX]:
            most_recent = pcd
    
    return os.path.join(POINTCLOUD_PATH, most_recent[NAME_INDEX])
        

def get_current_files(directory):
    """Iterates through the provided directory recording the 
    files and the last time they were modified. Is not recursive.

    Arguments:
        directory {string} -- A valid directory path

    Returns:
        Set -- The set of files stored in the given directory
    """
    files = set()
    for current_file in os.listdir(directory):
        fullpath=os.path.join(directory, current_file)
        if os.path.isfile(fullpath):
            files.add((current_file, os.path.getmtime(fullpath)))
    return files

if __name__ == "__main__":
    print(get_pointcloud())
