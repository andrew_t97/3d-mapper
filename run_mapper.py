import ros_control.ros_commands as ros_commands
import servo_control.servo_movement as servo_movement
import exporting.pcd_tools as pcd_tools
import time
import os
import signal
import argparse

if __name__ == "__main__":
    
    # Creating parser to allow user to select different execution options
    parser = argparse.ArgumentParser(description="3D maps the environment surrounding the camera")
    parser.add_argument("-r", '--rotations', metavar="ROTATIONS", dest="rots", type=int, default=2,
                help="""The number of rotations the camera will carry out, a single rotation is 
                where the camera rotates 360 degrees clockwise and 360 degrees anti-clockwise. 
                Larger values will result in a more accurate scan, however, you will get dimishing 
                returns with each increase. The default number of rotations is 2""")
    parser.add_argument("-s", "--servoport", dest="servo_port", default="/dev/ttyUSB0",
                        help="Specifies the port the servo (Arduino) is mounted upon, the default is /dev/ttyUSB0")
    parser.add_argument("-v", "--view", action="store_true", help="Renders the pointcloud and any meshes created")
    parser.add_argument("-d", "--debug", action="store_true", help="Shows debug information about rtabmap")
    
    args = parser.parse_args()
    number_of_rotations = args.rots
    view = args.view
    servo_port = args.servo_port
    debug = args.debug
    realsense_cmd = None
    rtabmap_cmd = None

    try:
        servo = servo_movement.Servo_Movement(servo_port)
        # Running ros programs
        realsense_cmd = ros_commands.run_realsense()
        time.sleep(5) # Sleeping to allow the process to setup before continuing
        rtabmap_cmd = ros_commands.run_rtabmap(debug=debug)
        time.sleep(10)

        print("Carrying out rotations...")
        servo.rotate(number_of_rotations)
        print("Done!")

        print("Exporting pointcloud to pointclouds folder...")
        pcl_filepath = ros_commands.get_pointcloud()
        print("Done!")

        if view:
            exporter.view_pcd(pcd_filepath)

        # Closing the realsense and rtabmap processes
        print("Killing processes...")
        os.kill(realsense_cmd.pid, signal.SIGINT)
        os.kill(rtabmap_cmd.pid, signal.SIGINT)
        print("Done!")

    except KeyboardInterrupt:
        if realsense_cmd:
            os.kill(realsense_cmd.pid, signal.SIGINT)
        if rtabmap_cmd:
            os.kill(rtabmap_cmd.pid, signal.SIGINT)

